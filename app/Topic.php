<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{

    function replies()
    {
        return $this->hasMany(Reply::class);
    }

    function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }


}
