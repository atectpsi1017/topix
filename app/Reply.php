<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    function topic()
    {
        return $this->belongsTo(Topic::class);
    }

    function user()
    {
        return $this->belongsTo(User::class);
    }
}
