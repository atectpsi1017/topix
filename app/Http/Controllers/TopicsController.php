<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Topic;

class TopicsController extends Controller
{

    function index()
    {
        return view('topics.index');
    }

    function store(Request $request)
    {
        $topic = new Topic();
        $topic->title = $request->get('text');
        $topic->user_id = auth()->user()->id;
        $topic->is_active = true;
        $topic->save();

        return redirect("/topics/{$topic->id}");
    }

    function show($id)
    {
        $topic = Topic::findOrFail($id);
        $replies = $topic->replies()->orderBy("created_at", "desc")->paginate(10);

        return view("topics.show")
            ->with(compact("topic"))
            ->with(compact("replies"));
    }

    function enable($id)
    {
        $topic = Topic::findOrFail($id);
        $topic->is_active = true;
        $topic->save();

        return redirect()->back();
    }

    function disable($id)
    {
        $topic = Topic::findOrFail($id);
        $topic->is_active = false;
        $topic->save();

        return redirect()->back();
    }
}
