<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Topic;
use App\Reply;

class RepliesController extends Controller
{
    function store($id, Request $request)
    {
        $topic = Topic::findOrFail($id);
        $reply = new Reply();
        $reply->text = $request->get("text");
        $reply->user_id = auth()->user()->id;
        $topic->replies()->save($reply);

        return redirect()->back();
    }
}
