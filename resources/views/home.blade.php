@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Topics
                    <span class="float-right">
                        <a href="/topics/create" class="btn btn-sm btn-info">Create</a>
                    </span>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div>
                        @forelse($topics as $topic)
                        <p>
                            <a href="/topics/{{$topic->id}}">{{ $topic->title }}</a>
                            <span class="float-right">
                                @if($topic->is_active)
                                <span class="badge badge-success">Active</span>
                                @else
                                <span class="badge badge-danger">Inactive</span>
                                @endif
                            </span>
                        </p>
                        <hr>
                        @empty
                        <div style="text-align: center; font-weight: bold;">
                            <p>No topics created yet!</p>
                            Click <a href="/topics/create">here</a> to create the first topic.
                        </div>
                        @endforelse
                    </div>
                    <div>
                        {{ $topics->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
