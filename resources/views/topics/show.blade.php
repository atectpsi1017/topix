@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Topic #{{$topic->id}}
                    <span class="float-right">
                    @if($topic->author->id == auth()->user()->id)
                        @if($topic->is_active)
                            <a href="/topics/{{$topic->id}}/disable" class="btn btn-sm btn-danger">Disable</a>
                        @else
                            <a href="/topics/{{$topic->id}}/enable" class="btn btn-sm btn-success">Enable</a>
                        @endif
                    @endif
                    </span>

                </div>
                <div class="card-body">
                    {{ $topic->title }}
                </div>
            </div>
            <p></p>
            <div class="card">
                <div class="card-header">Replies</div>
                <ul class="list-group list-group-flush">
                @forelse($replies as $reply)
                    <li class="list-group-item">
                        {{ $reply->text }}
                    </li>
                @empty
                    <div style="text-align: center; font-weight: bold;">
                        <p>Be the first to comment to this topic!</p>
                    </div>
                @endforelse
                </ul>
                <div>
                    {{ $replies->links() }}
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    @if($topic->is_active)
                    <form action="/topics/{{$topic->id}}/reply" method="post">
                    @csrf()
                     <div class="form-group">
                        <textarea required class="form-control" id="text" name="text" rows="3"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Reply</button>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
