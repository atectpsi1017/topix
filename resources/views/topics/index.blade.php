@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">New topic</div>
                <div class="card-body">
                    <form action="/topics" method="post">
                    @csrf()
                    <div class="form-group">
                        <label for="text">Text</label>
                        <textarea required class="form-control" id="text" name="text" rows="3"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
