<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/topics/create', 'TopicsController@index');
Route::post('/topics', 'TopicsController@store');
Route::get('/topics/{id}', 'TopicsController@show');
Route::post('/topics/{id}/reply', 'RepliesController@store');
Route::get('/topics/{id}/enable', 'TopicsController@enable');
Route::get('/topics/{id}/disable', 'TopicsController@disable');
